import colorful as cf
from dataclasses import dataclass
import importlib.util as importutil
import inspect
import logging
import os


_log = logging.getLogger(__name__)

SCOPES = ["function", "file"]

_FIXTURES = {}

@dataclass
class Fixture:
    name: str
    scope: str
    f: ...
    gen: ... = None
    value: ... = None

class FixtureState:
    def __init__(self):
        self.fixtures = {}

    def add(self, fixture):
        self.fixtures[fixture.name] = fixture

    def reset(self, scope):
        scope_pos = SCOPES.index(scope)
        for fixture in self.fixtures.values():  # FIXME: reverse order of setup!
            if SCOPES.index(fixture.scope) <= scope_pos and fixture.gen:
                _log.debug("fixture %s: teardown", fixture.name)
                try:
                    next(fixture.gen)
                except StopIteration:
                    pass
                _log.debug("fixture %s: teardown complete", fixture.name)
                fixture.gen = None
                fixture.value = None

    def resolve(self, func):
        args = []
        for arg_name in inspect.getfullargspec(func).args:
            fixture = self.fixtures.get(arg_name)
            if not fixture:
                raise KeyError(f"unknown fixture {arg_name}")
            if fixture.value is None:
                if fixture.gen is None:
                    _log.debug("fixture %s: prepare", fixture.name)
                    fixture_args = self.resolve(fixture.f)
                    _log.debug("fixture %s: args %s", fixture.name, fixture_args)
                    fixture.gen = fixture.f(*fixture_args)
                _log.debug("fixture %s: setup", fixture.name)
                fixture.value = next(fixture.gen)
                _log.debug("fixture %s: value %s", fixture.name, fixture.value)
            args.append(fixture.value)
        return args


_FIXTURES = FixtureState()


def fixture(scope="function", name=None):
    # TODO if callable(score) -> f=scope, scope="function", shortcut
    def _decorator(f):
        def _f(*args, **kwargs):
            return f(*args, **kwargs)
        f_name = name or f.__name__
        _log.info("micropytest.fixture %s = %r", f_name, f)
        _FIXTURES.add(Fixture(f_name, scope, f))
        return _f
    return _decorator


def invoke(func):
    args = _FIXTURES.resolve(func)
    _log.debug("calling %s(%s)", func, args)
    try:
        return func(*args)
    finally:
        _FIXTURES.reset("function")


def run_tests(testfiles, abort_on_error=False):
    tests = []
    stats = {"total": 0, "okay": 0, "fail": 0, "error": 0}
    for testfile in testfiles:
        basename = os.path.basename(testfile)
        if basename != "conftest.py" and not (basename.startswith("test_") and basename.endswith(".py")):
            continue
        modname = basename[:-3]  # strip .py
        print(cf.yellow(f"processing: {testfile}"))
        spec = importutil.spec_from_file_location(modname, testfile)
        testmod = importutil.module_from_spec(spec)
        spec.loader.exec_module(testmod)
        for identifier, member in inspect.getmembers(testmod):
            if inspect.isfunction(member) and identifier.startswith("test_"):
                _log.info("running test: %s %s", modname, identifier)
                try:
                    stats["total"] += 1
                    invoke(member)
                    tests.append((basename, identifier, None))
                    stats["okay"] += 1
                    print(cf.yellow(f"{modname}.{identifier} ") + cf.green("OK"))
                except AssertionError as ex:
                    tests.append((basename, identifier, ex.args[0]))
                    stats["fail"] += 1
                    print(cf.yellow(f"{modname}.{identifier} ") + cf.red("FAIL ") + ex.args[0])
                except Exception as ex:
                    tests.append((basename, identifier, ex))
                    stats["error"] += 1
                    print(cf.yellow(f"{modname}.{identifier} ") + cf.magenta("ERROR ") + repr(ex))
                    if abort_on_error:
                        raise
        _FIXTURES.reset("file")
    print(cf.yellow(f"{stats['total']} tests run: {stats['okay']} ({100*stats['okay']/stats['total']}%) ok, {stats['fail']} failed, {stats['error']} errors"))
    return tests
