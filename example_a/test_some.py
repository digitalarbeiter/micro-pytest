import micropytest


def test_without_fixture():
    print("test_without_fixture()")


def test_fails():
    assert 1 == 0, "le wilde assertion appears"


def test_error():
    raise ValueError("1==1")


def test_database(connection_pool_factory):
    print(f"test_database(connection_pool_factory={connection_pool_factory})")


def test_offers_db(offers_db):
    print(f"test_offers_db(offers_db={offers_db})")
    offers_db.insert(
        "offers",
        [
            {"id": 1, "name": "Hello"},
            {"id": 2, "name": "world"}
        ],
    )
    assert offers_db.select(
        "offers",
        where=lambda row: row["id"]==1
    )[0]["id"] == 1
    print("test_offers_db() succeeded")
