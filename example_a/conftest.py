import functools as ft
from micropytest import fixture


class Database:
    def __init__(self, db_name, tables):
        self._name = db_name
        self.tables = {table: {} for table in tables}

    def __str__(self):
        return f"database {self._name}, tables: {self.tables.keys()}"

    def insert(self, table, rows):
        for row in rows:
            self.tables[table][row["id"]] = row

    def select(self, table, where):
        return [row for row in self.tables[table].values() if where(row)]


_databases = {}  # TODO: next step: move this into fixtures and let µpytest worry about it

@fixture()
def connection_pool_factory():
    print("connection_pool_factory() setup")
    yield _databases
    print("connection_pool_factory() teardown")


def generic_database(connection_pool_factory, db_name=None, tables=None):
    # TODO: alembic-style _models.Base.metadata.create_all()/drop_all()
    print(f"generic_database({db_name}) setup")
    database = Database(db_name, tables)
    _databases[db_name] = database
    yield database
    print(f"generic_database({db_name}) teardown")
    _databases.pop(db_name)


offers_db = ft.partial(generic_database, db_name="offersdb", tables=["offers", "product_update_queue"])
fixture(name="offers_db")(offers_db)
